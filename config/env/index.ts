import { env as dev } from './env.dev';
import { env as prod } from './env.prod';

const { UMI_ENV = 'dev' } = process.env;
const environments = { dev, prod };

export interface IEnvironment {
  apiUrl?: string;
  theme?: {};
}

export default environments[UMI_ENV];
