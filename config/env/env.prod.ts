import { IEnvironment } from '.';

export const env: IEnvironment = {
  apiUrl: 'https://localhost:8000',
};
