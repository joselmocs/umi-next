export default [
  {
    path: '/demo',
    component: '../layouts/basic',
    routes: [
      {
        path: '/demo',
        component: './demo',
        title: 'demonstration page',
      },
    ],
  },
  {
    path: '/',
    component: '../layouts/blank',
    routes: [
      {
        path: '/',
        component: './blank',
        title: 'blank page',
      },
    ],
  },
];
