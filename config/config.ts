import { defineConfig } from 'umi';
import environment from './env';
import routes from './routes';

const { UMI_ENV = 'dev' } = process.env;

export default defineConfig({
  define: {
    UMI_ENV,
  },
  devServer: {
    port: 3000,
    http2: true,
  },
  dva: {
    hmr: true,
  },
  hash: true,
  manifest: {
    basePath: '/',
  },
  mock: false,
  routes,
  targets: {
    ie: 11,
  },
  theme: environment.theme,
  title: 'umi-next',
});
