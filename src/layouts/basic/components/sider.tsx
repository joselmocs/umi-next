import React from 'react';
import { Drawer, Layout } from 'antd';

import { BasicLayoutProps } from '../index';
import { MenuTree } from './menu';

import styles from './index.less';

const Sider = ({ route, location, isCollapsed, isMobile }: BasicLayoutProps) => {
  return (
    <Layout.Sider collapsible width={256} trigger={null} collapsed={isCollapsed}>
      <div className={styles.logo}>
        <span>
          <img src="/logo.svg" alt="logo" />
          {!isCollapsed && <h1>Projeto</h1>}
        </span>
      </div>
      <MenuTree route={route} location={location} isCollapsed={isCollapsed} isMobile={isMobile} />
    </Layout.Sider>
  );
};

export const SiderWrapper = (props: BasicLayoutProps) => {
  const { route, location, isMobile, isCollapsed, setIsCollapsed } = props;

  if (!isMobile) {
    return (
      <Sider route={route} location={location} isCollapsed={isCollapsed} isMobile={isMobile} />
    );
  }

  return (
    <Drawer
      visible={!isCollapsed}
      placement="left"
      onClose={() => setIsCollapsed && setIsCollapsed(true)}
      width={256}
      bodyStyle={{ padding: 0 }}
    >
      <Sider route={route} location={location} isCollapsed={isCollapsed} isMobile={isMobile} />
    </Drawer>
  );
};
