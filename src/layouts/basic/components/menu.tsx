import React from 'react';
import { history } from 'umi';
import { Menu } from 'antd';
import { pathToRegexp } from 'path-to-regexp';

import { BasicLayoutProps } from '../index';

interface RouteMenu {
  icon?: React.ReactNode;
  key: string;
  name: string;
  parentKeys: string[];
  path?: string;
  routes?: RouteMenu[];
}

export function omit(obj: any, fields: string[]) {
  const shallowCopy = { ...obj };
  for (let i = 0; i < fields.length; i += 1) {
    delete shallowCopy[fields[i]];
  }
  return shallowCopy;
}

export const getCurrentRouteMenu = (
  path: string,
  routes: RouteMenu[],
  matchs: RouteMenu[] = [],
) => {
  routes.forEach((route) => {
    if (!route.path && route.routes) {
      matchs.concat(getCurrentRouteMenu(path, route.routes, matchs));
    }
    if (route.path === path || pathToRegexp(`${route.path}`).test(path)) {
      matchs.push(route);
    }
  });

  return matchs;
};

const getRouteMenusFromRoutes = (routes: RouteMenu[], parent?: RouteMenu): RouteMenu[] => {
  return routes
    .filter((route) => route.name)
    .map((route, key) => {
      const routeMenu: RouteMenu = omit(route, ['component', 'title', 'exact']);

      routeMenu.key = parent ? `${parent.key}-${key}` : `${key}`;
      routeMenu.parentKeys = parent ? [...parent.parentKeys, parent.key] : [];

      if (route.routes) {
        routeMenu.routes = getRouteMenusFromRoutes(route.routes, routeMenu);
      }
      return routeMenu;
    });
};

const renderMenuItem = (route: RouteMenu): React.ReactNode => (
  <span>
    <span>{route.name}</span>
  </span>
);

const menuItemOnClick = (route: RouteMenu) => {
  return route.path && history.push(route.path);
};

const renderRouteMenu = (route: RouteMenu) => {
  if (route.routes) {
    return (
      <Menu.SubMenu key={route.key} title={renderMenuItem(route)}>
        {route.routes.map(renderRouteMenu)}
      </Menu.SubMenu>
    );
  }
  return (
    <Menu.Item key={route.key} onClick={() => menuItemOnClick(route)}>
      {renderMenuItem(route)}
    </Menu.Item>
  );
};

interface MenuTreeState {
  pathname?: string;
  routeMenus: RouteMenu[];
  currentRouteMenu?: RouteMenu;
}

export class MenuTree extends React.Component<BasicLayoutProps, MenuTreeState> {
  state = {
    pathname: undefined,
    routeMenus: getRouteMenusFromRoutes(this.props.route.routes),
    currentRouteMenu: undefined,
  };

  static getDerivedStateFromProps(props: any, state: any) {
    if (state.pathname === props.location.pathname) {
      return null;
    }

    return {
      pathname: props.location.pathname,
      currentRouteMenu: getCurrentRouteMenu(props.location.pathname, state.routeMenus).pop(),
    };
  }

  shouldComponentUpdate(nextProps: any) {
    return (
      nextProps.location.pathname !== this.props.location.pathname ||
      nextProps.isCollapsed !== this.props.isCollapsed ||
      nextProps.isMobile !== this.props.isMobile
    );
  }

  render() {
    const { currentRouteMenu, routeMenus } = this.state;
    const { isCollapsed, isMobile } = this.props;

    const mode = isCollapsed && !isMobile ? 'vertical' : 'inline';
    const openKeys = isCollapsed && !isMobile ? [] : currentRouteMenu?.parentKeys;
    const selectedKey = currentRouteMenu?.key || [];

    return (
      <Menu theme="dark" selectedKeys={[selectedKey]} defaultOpenKeys={openKeys} mode={mode}>
        {routeMenus && routeMenus.map(renderRouteMenu)}
      </Menu>
    );
  }
}
