import React from 'react';
import { Layout } from 'antd';
import { MenuFoldOutlined, MenuUnfoldOutlined } from '@ant-design/icons';

import { BasicLayoutProps } from '../index';

import styles from './index.less';

export const Header = (props: BasicLayoutProps) => {
  const { isMobile, isCollapsed, setIsCollapsed } = props;

  return (
    <Layout.Header>
      {isMobile && (
        <span className={styles.headerlogo}>
          <img src="/logo.svg" alt="logo" />
        </span>
      )}
      <span
        className={styles.trigger}
        onClick={() => setIsCollapsed && setIsCollapsed(!isCollapsed)}
      >
        {isCollapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
      </span>
    </Layout.Header>
  );
};
