import React, { useState, useLayoutEffect } from 'react';
import { Layout } from 'antd';

import { Header, SiderWrapper } from './components';

export interface BasicLayoutProps {
  isMobile?: boolean;
  isCollapsed?: boolean;
  setIsCollapsed?(state: boolean): void;
  route?: any;
  location?: any;
}

const getScreenMedia = () => [
  window.matchMedia('(max-width: 767px)').matches,
  window.matchMedia('(max-width: 1199px)').matches,
];

const BasicLayout: React.FC = (props: any) => {
  const { children, route, location } = props;

  let [isScreenSM, isScreenXL] = getScreenMedia();

  const [isMobile, setIsMobile] = useState(isScreenSM);
  const [isCollapsed, setIsCollapsed] = useState(isScreenXL);

  const handleEventResize = () => {
    [isScreenSM, isScreenXL] = getScreenMedia();

    setIsCollapsed(isScreenSM ? true : isScreenXL);
    setIsMobile(isScreenSM);
  };

  useLayoutEffect(() => {
    window.addEventListener('resize', handleEventResize);

    return () => window.removeEventListener('resize', handleEventResize);
  }, [isScreenSM, isScreenXL]);

  return (
    <Layout>
      <SiderWrapper
        isMobile={isMobile}
        isCollapsed={isCollapsed}
        setIsCollapsed={setIsCollapsed}
        location={location}
        route={route}
      />
      <Layout>
        <Header isMobile={isMobile} isCollapsed={isCollapsed} setIsCollapsed={setIsCollapsed} />
        <Layout.Content>{children}</Layout.Content>
      </Layout>
    </Layout>
  );
};

export default BasicLayout;
