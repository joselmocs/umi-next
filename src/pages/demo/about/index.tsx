import React from 'react';
import { Link } from 'umi';

export default () => {
  return (
    <div style={{ paddingTop: '4em', textAlign: 'center' }}>
      <div>About Page!</div>
      <Link to="/demo">go to /demo</Link>
    </div>
  );
};
