import React from 'react';
import { Link } from 'umi';

export default () => {
  return (
    <div style={{ paddingTop: '4em', textAlign: 'center' }}>
      <div>Demonstration Page!</div>
      <Link to="/demo/about">go to /demo/about</Link>
    </div>
  );
};
