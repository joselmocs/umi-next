import React from 'react';
import { Link } from 'umi';

export default () => (
  <div style={{ paddingTop: '4em', textAlign: 'center' }}>
    <Link to="/demo">go to /demo</Link>
  </div>
);
